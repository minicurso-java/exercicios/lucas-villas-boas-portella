import animais.*;

import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        porco meuporco = new porco();
        meuporco.correr();
        meuporco.comer();
        meuporco.emitirsom();
        vaca minhavaca = new vaca();
        minhavaca.correr();
        minhavaca.comer();
        minhavaca.emitirsom();
        cavalo meucavalo = new cavalo();
        meucavalo.correr();
        meucavalo.comer();
        meucavalo.emitirsom();
        ema minhaema = new ema();
        minhaema.correr();
        minhaema.comer();
        minhaema.emitirsom();
        coiote meucoiote = new coiote();
        meucoiote.correr();
        meucoiote.comer();
        meucoiote.emitirsom();
    }
}