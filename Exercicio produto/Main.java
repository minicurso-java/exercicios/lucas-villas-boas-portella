import Mercado.Produto;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Produto produto = new Produto();
        System.out.println("Digite o nome do produto: ");
        produto.nome = sc.nextLine();
        System.out.println("Digite o quantidade do produto: ");
        produto.quantidade = sc.nextInt();
        System.out.println("Digite o valor da produto: ");
        produto.preco = sc.nextDouble();
        produto.addproduto(sc.nextInt());
        produto.removeproduto(sc.nextInt());
        produto.valortotal();
    }
}
