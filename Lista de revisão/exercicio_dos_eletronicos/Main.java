import Eletronicos.Notebook;
import Eletronicos.Smartphone;
import Eletronicos.Tablet;

public class Main {
    public static void main(String[] args) {
        Notebook x = new Notebook();
        x.definirMarca();
        x.definirAno();
        x.ligarComputador();
        x.escrever();
        Smartphone y = new Smartphone();
        y.definirMarca();
        y.definirAno();
        y.ligarCelular();
        y.notificação();
        Tablet z = new Tablet();
        z.definirMarca();
        z.definirAno();
        z.ligarTablet();
        z.tocarVideo();
    }
}