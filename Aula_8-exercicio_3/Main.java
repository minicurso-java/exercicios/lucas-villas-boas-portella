import entidades.Aluno;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Aluno x = new Aluno();
        System.out.println("informe o nome e as notas do aluno: ");
        x.nome = sc.nextLine();
        x.nota1 = sc.nextDouble();
        x.nota2 = sc.nextDouble();
        x.nota3 = sc.nextDouble();
        x.media = x.calcularMedia(x.nota1, x.nota2, x.nota3);
        System.out.println("Nota final: "+ x.media);
        if (x.media>60) {
            System.out.println("Passou!");
        } else {
            System.out.println("Não Passou!");
            double i = 60 - x.media;
            System.out.println("Faltou "+ i +" pontos");
        }
    }
}