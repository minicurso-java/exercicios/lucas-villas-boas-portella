import java.util.Scanner;
import Banco.ContaBancaria;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ContaBancaria conta = new ContaBancaria();
        conta.depositar(sc.nextInt());
        conta.sacar(sc.nextInt());
        conta.verificarSaldo();
    }
}
