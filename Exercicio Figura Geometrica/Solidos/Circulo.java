package Solidos;

public class Circulo extends FiguraGeometrica{
    public double raio;

    public void calcarea() {
        System.out.println(Math.PI * Math.pow(raio, 2));
    }
    public void calcperimetro() {
        System.out.println(2 * Math.PI * raio);
    }
}
