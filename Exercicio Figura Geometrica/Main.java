import java.util.Scanner;

import Solidos.Circulo;
import Solidos.FiguraGeometrica;
import Solidos.Quadrado;
import Solidos.Triangulo;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Quadrado x = new Quadrado();
        x.lado = sc.nextDouble();
        x.calcarea();
        x.calcperimetro();
        Triangulo y = new Triangulo();
        y.base = sc.nextDouble();
        y.altura = sc.nextDouble();
        y.calcarea();
        y.calcperimetro();
        Circulo z = new Circulo();
        z.raio = sc.nextDouble();
        z.calcarea();
        z.calcperimetro();
    }
}